import enum
import functools
from typing import Callable

from typer import prompt
from click.exceptions import Abort

from game.models import Grid, Color

MAX_MOVES_NUMBER = 21


class UserInput(str, enum.Enum):
    red = "r"
    yellow = "y"
    green = "g"
    blue = "b"


def catch_exit(fn: Callable | None = None, msg: str = "Thank you for the game"):
    if fn is None:
        return functools.partial(catch_exit, msg=msg)

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except (KeyboardInterrupt, Abort):
            print(f"\n{msg}")
            exit(0)

    return wrapper


class Game:
    def __init__(self, grid: Grid) -> None:
        self._grid = grid

    @catch_exit
    def run(self) -> None:
        print(self._grid)
        for move_number in range(0, MAX_MOVES_NUMBER):
            user_input: UserInput = prompt(
                text=f"{move_number}/{MAX_MOVES_NUMBER} Moves. Insert color",
                type=UserInput,
            )
            self._grid.update_color(new_color=Color[user_input.name])
            if self._grid.monolithic:
                break
            print(self._grid)

        winner = self._grid.monolithic
        if winner:
            print("You won!")
        else:
            print("You lose")
