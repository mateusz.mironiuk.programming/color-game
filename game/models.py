from __future__ import annotations

import enum
import random
import typing


class Color(enum.Enum):
    blue = "\033[34m"
    red = "\033[31m"
    green = "\033[32m"
    yellow = "\033[33m"

    @classmethod
    def random(cls) -> Color:
        return random.choice((cls.blue, cls.red, cls.green, cls.yellow))

    def __str__(self) -> str:
        return f"{self.value}█\033[0m"


class Cell:
    __slots__ = ["_color"]

    def __init__(self, color: Color) -> None:
        if not isinstance(color, Color):
            raise TypeError(f"{Color!r} required")
        self._color = color

    def __str__(self) -> str:
        return str(self._color)

    __repr__ = __str__

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color: Color) -> None:
        if not isinstance(color, Color):
            raise TypeError(f"{Color!r} required")
        self._color = color

    @classmethod
    def random(cls) -> Cell:
        return cls(color=Color.random())


Row: typing.TypeAlias = int
Column: typing.TypeAlias = int
Coords: typing.TypeAlias = tuple[Row, Column]

DEFAULT_ROWS = 18
DEFAULT_COLS = 18

STARTING_CELL_COORDS = (0, 0)


class Grid:
    def __init__(self, rows: int = DEFAULT_ROWS, cols: int = DEFAULT_COLS) -> None:
        self._rows = rows
        self._cols = cols
        self._grid = [[Cell.random() for _ in range(self._cols)] for _ in range(self._rows)]

    def __setitem__(self, coords: Coords, color: Color) -> None:
        if (
            not isinstance(coords, tuple)
            and len(coords) != 2
            and not all(isinstance(coord, int) for coord in coords)
        ):
            raise TypeError
        row, col = coords
        self._grid[row][col].color = color

    def __getitem__(self, coords: Coords) -> Cell:
        if (
            not isinstance(coords, tuple)
            and len(coords) != 2
            and not all(isinstance(coord, int) for coord in coords)
        ):
            raise TypeError
        row, col = coords
        return self._grid[row][col]

    def __repr__(self) -> str:
        return "\n".join(
            "".join(str(square) for square in row)
            for row in self._grid
        )

    __str__ = __repr__

    def _set_color(self, coords: Coords, color: Color) -> None:
        self[coords].color = color

    def _get_color(self, coords: Coords) -> Color:
        return self[coords].color

    def update_color(self, new_color: Color) -> None:
        """Set new color at STARTING_CELL_COORDS and update all adjacent squares accordingly"""
        old_color = self._get_color(STARTING_CELL_COORDS)
        visited: set[Coords] = set()
        self._set_color(STARTING_CELL_COORDS, color=new_color)
        self._spread_color(
            coords=STARTING_CELL_COORDS,
            old_color=old_color,
            new_color=new_color,
            visited=visited
        )

    def _spread_color(
        self,
        coords: Coords,
        old_color: Color,
        new_color: Color,
        visited: set[Coords]
    ) -> None:
        row, col = coords
        visited.add(coords)
        neighbors_coords: list[Coords] = [
            (new_row, new_col) for new_row, new_col in (
                (row, col + 1),
                (row, col - 1),
                (row + 1, col),
                (row - 1, col),
            )
            if (new_row in range(self._rows))
            and (new_col in range(self._cols))
            and ((new_row, new_col) not in visited)
        ]
        for coords in neighbors_coords:
            if self._get_color(coords) == old_color:
                self._set_color(coords, color=new_color)
                self._spread_color(
                    coords=coords,
                    old_color=old_color,
                    new_color=new_color,
                    visited=visited,
                )

    @property
    def monolithic(self) -> bool:
        colors = [square.color for row in self._grid for square in row]
        return all(color == colors[0] for color in colors)
