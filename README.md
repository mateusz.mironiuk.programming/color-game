## Color Game

Terminal based color squares game.

![Screen of game in progress](bin/screen.png?raw=true "Game in progress")

### prerequisites

- `python3.10` installed

### installation

make sure you have right python version installed:

```
$ python --version
Python 3.10.4
```

crate and acquire virtual environment

```
$ python -m venv venv && source venv/bin/activate
```

install dependencies:

```
$ pip install poetry && poetry install
``` 

### running game

run in terminal

```
$ python main.py
```