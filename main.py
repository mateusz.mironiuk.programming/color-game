from game.game import Game
from game.models import Grid


if __name__ == '__main__':
    Game(grid=Grid()).run()
